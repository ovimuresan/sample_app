Rails.application.routes.draw do
  
  root 'static_pages#home'
  
  get  '/help',    to: 'static_pages#help'

  get  '/about',   to: 'static_pages#about'

  get  '/contact', to: 'static_pages#contact'
  
  get  '/signup',  to: 'users#new'
  
  get    '/login',   to: 'sessions#new'
  
  post   '/login',   to: 'sessions#create'
  
  delete '/logout',  to: 'sessions#destroy'
#HTTP request	URL	        Action	           Named route  
# GET	/users/1/following	following	following_user_path(1)
# GET	/users/1/followers	followers	followers_user_path(1)

resources :users do
  member do
    get :following, :followers
  end
end
  
  resources :account_activations, only: [:edit]
  
  resources :microposts,          only: [:create, :destroy]
  
  resources :relationships,       only: [:create, :destroy]
  
end
